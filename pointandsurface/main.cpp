#include <iostream>

using namespace std;
double A,B,C,D,X,Y,Z=0;

int main()
{
    cout << "Ax+By+Cz+D=0"<<endl;
    cout << "A=";
    cin >> A;
    cout << "B=";
    cin >> B;
    cout << "C=";
    cin >> C;
    cout << "D=";
    cin >> D;
    cout << "Point(X,Y,Z)"<<endl;
    cout << "X=";
    cin >> X;
    cout << "Y=";
    cin >> Y;
    cout << "Z=";
    cin >> Z;
    if(C==0)
    {
        cout << "Surface is vertical";
    }
    else
    {
        cout << "Point is ";
        C=-(D+A*X+B*Y)/C; // Now C is z(X,Y) coordinate of surface
        if(Z==C)
        {
            cout << "on";
        }
        else if(Z>C)
        {
            cout << "above";
        }
        else
        {
            cout << "below";
        }
        cout << " the surface";
    }

    return 0;
}
