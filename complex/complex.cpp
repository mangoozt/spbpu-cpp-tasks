#include "complex.h"

complex::complex(double Re, double Im)
{
    m_y=Im;
    m_x=Re;
}

complex operator+(const complex &a,const complex &b)
{
    return complex(a.m_x+b.m_x,a.m_y+b.m_y);
}
complex operator+(const complex &a)
{
    return complex(a.m_x,a.m_y);
}

complex operator-(const complex &a,const complex &b)
{
    return complex(a.m_x-b.m_x,a.m_y-b.m_y);
}
complex operator-(const complex &a)
{
    return complex(-a.m_x,-a.m_y);
}
complex operator*(const complex &a,const complex &b)
{
    // (a1+ib1)*(a2+ib2)=(a1*a2-b1*b2)+i(b1*a2+b2*a1)
    return complex(a.m_x*b.m_x-a.m_y*b.m_y,a.m_y*b.m_x+b.m_y*a.m_x);
}

complex operator/(const complex &a,const complex &b)
{
    // (a1+ib1)*(a2+ib2)=(a1*a2-b1*b2)+i(b1*a2+b2*a1)
    double d=1/(b.m_x*b.m_x+b.m_y*b.m_y);
    return complex((a.m_x*b.m_x+a.m_y*b.m_y)*d,(a.m_y*b.m_x-b.m_y*a.m_x)*d);
}

std::ostream& operator<<(std::ostream& os, const complex& c)
{
    if(c.m_x!=0){
        os << c.m_x;
        if(c.m_y>0)
            os << '+';
    }
    if(c.m_y!=0){
        if(c.m_y==-1) os << '-';
        else if(c.m_y!=1) os << c.m_y;
        os << 'i';
        }
	if(c.m_x==0&&c.m_y==0) os << '0';
    return os;
};

double complex::abs() const
{
    return sqrt(this->m_x*this->m_x+this->m_y*this->m_y);
}

double complex::arg() const
{
    if(this->m_x>0)
        return atan(this->m_y/this->getRe());
    else if(this->m_x<0)
    {
        if(this->m_y>=0) return atan(this->m_y/this->m_x)+c_PI;
        else return atan(this->m_y/this->m_x)-c_PI;
    }
    else    // Re==0
    {
        if(this->m_y>0) return c_PI/2;
        else if(this->m_y<0) return -c_PI/2;
        else return 0; // Let it be zero instead of indeterminate
    }
}
void complex::arg(double Arg)
{
    double abs=this->abs();
    this->m_x=abs*cos(Arg);
    this->m_y=abs*sin(Arg);
}
double abs(const complex& c)
{
    return c.abs();
}
void complex::abs(double Abs)
{
    double arg=this->arg();
    this->m_x=Abs*cos(arg);
    this->m_y=Abs*sin(arg);
}
