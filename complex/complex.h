#ifndef COMPLEX_H
#include <iostream>
#include <math.h>
#define COMPLEX_H

class complex
{
public:
    complex(double Re=0, double Im=0);

    friend complex operator+(const complex &a,const complex &b);
    friend complex operator+(const complex &a);
    friend complex operator-(const complex &a,const complex &b);
    friend complex operator-(const complex &a);
    friend complex operator*(const complex &a,const complex &b);
    friend complex operator/(const complex &a,const complex &b);
    friend std::ostream& operator<<(std::ostream& os, const complex& c);
    friend double abs(const complex& c);

    double getRe()const
    {
        return m_x;
    }
    void setRe(double Re)
    {
        m_x=Re;
    }
    double getIm()const
    {
        return m_y;
    }
    void setIm(double Im)
    {
        m_y=Im;
    }

    double abs() const;
    void abs(double Abs);
    double arg() const;
    void arg(double Arg);


private:
    double m_y;
    double m_x;
};
const complex c_i(0,1);
const double c_PI  =3.141592653589793238463;
#endif // COMPLEX_H
