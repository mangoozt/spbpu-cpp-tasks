#include <iostream>
#include "complex.h"
#include <stdlib.h>
#if __cplusplus!=201103L
#error An ISO C++ 2011 compiler needed.
#endif // __cplusplus
using namespace std;


complex a;
complex b;
complex c;
double d;

int main()
{
    a={2,3};
    cout << "a="<< a << endl;
    b={5,2};
    cout << "b="<< b << endl;
    c=a-b;
    cout << "c=a-b="<< c << endl;
    c=c-1+2*c_i;
    cout << "c=c-1+2i="<< c << endl;
    cout << "abs(c)="<< abs(c) << endl;
    cout << "c.arg()="<< c.arg() << "rad="<< c.arg()*180/c_PI << "deg" << endl;
    c=5-5*c_i;
    cout << "c="<< c << endl;
    cout << "abs(c)="<< abs(c) << endl;
    cout << "c.arg()="<< c.arg() << "rad="<< c.arg()*180/c_PI << "deg" << endl;
    cout << "i="<< c_i << endl;
    cout << "i-i=" << c_i-c_i << endl;
    cout << "-1*i="<< -1*c_i << endl;
    cout << "2+1*i="<< 2+1*c_i << endl;
    cout << "i*i="<< c_i*c_i << endl;
    return 0;
}
