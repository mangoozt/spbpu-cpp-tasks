#include <iostream>
#include <stdlib.h>
using namespace std;
double a,b;
char operation;
int main()
{
    cout << "Possible operations : +,-,*,/,gcd (g),lcm (l)."<<endl<<"Format: A[+-*/gl]B." <<endl<<"Violate the format for exit."<<endl;
    for(;;)
    {
        cout << "> ";
        cin >> a;
        if(!cin) exit(0);
        cin >> operation;
        cin >> b;
        if(!cin) exit(0);
        switch(operation)
        {
        case '-':
            a-=b;
            break;
        case '+':
            a+=b;
            break;
        case '/':
            if(b==0)
            {
                operation=0;
                break;
            }
            a/=b;
            break;
        case '*':
            a*=b;
            break;
        case '^':
        {
            unsigned int n=abs(b);
            if(n==0)
            {
                if(a==0)
                {
                    operation=0;    // 0^0 Exception
                    break;
                }
                a=1;
                break; // skip for a^0=1
            }
            if(a==1)break; // skip for 1^b=1
            if(a==0)
            {
                if(b>0) break;            // skip for 0^b
                else
                {
                    operation=0;    // Div by zero Exception
                    break;
                }
            }
            double r=1;
            if(a==2)
            {
                r=2<<(n-1);
            }
            else
            {
                while(n!=1)
                {
                    if(n&1) r*=a;
                    n>>=1;
                    a*=a;
                }
                r*=a;
            }
            if(b>0)a=r;
            else a=1/r;
            break;
        }
        case 'l':
        case 'g':
        {
            unsigned int m=abs(a);
            unsigned int n=abs(b);
            unsigned int r=0;
            if((m==0 && n==0) || (operation=='l'&&(m==0||n==0)))
            {
                operation=0;
                break;
            }
            for(;;)
            {
                if(n==1||m==1)
                {
                    m=1;    // gcd(1,n)=gcd(n,1)=1
                    break;
                }
                if(m==n)
                {
                    break;   // gcd(n,n)=n
                }
                if(n>m)                  // m>n
                {
                    m^=n;
                    n^=m;
                    m^=n;
                }
                if(n==0)
                {
                    break;   // gcd(m,0)=m
                }
                if(!(m&1) && !(n&1))   // gcd(m,n)=2*gcd(m/2,n/2) if n%2=0 and m%2=0
                {
                    r++;
                    m>>=1;
                    n>>=1;
                    continue;
                }
                if(!(m&1))
                {
                    m>>=1;    // gcd(m,n)=gcd(m/2,n) if m%2=0
                    continue;
                }
                if(!(n&1))
                {
                    n>>=1;    // gcd(m,n)=gcd(m,n/2) if n%2=0
                    continue;
                }
                m=(m-n)>>1;                               // gcd(m,n)=gcd((m-n)/2,n) if n%2=1 and m%2=1
            }
            cout << "=" << ((operation=='l') ? abs(int(a)*int(b)/(m<<r)) : (m<<r)) ;// lcm(m,n)=m*n/gcd(m,n)
            goto endfor;
        }
        default:
            exit(0);
        }
        if(operation!=0) cout << "=" << a;
        else cout<<"Undefined";
endfor:
        cout << endl << endl;
    }
}
